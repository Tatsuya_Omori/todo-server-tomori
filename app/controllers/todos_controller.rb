class TodosController < ApplicationController
  before_action :set_todo, only: [:update, :destroy]

  def index
    @todos = Todo.all.order(date: :asc)
    successful(@todos, action_name)
    rescue ActiveRecord::ActiveRecordError
      error_message(action_name)
    rescue StandardError
      render status: 500, json: { error_code: 1, error_message: "サーバー内で不明なエラーが発生しました" }
  end

  def create
    @todo = Todo.create!(todo_params)
    successful(@todo, action_name)
    rescue ActiveRecord::RecordInvalid
      error_validate()
    rescue ActiveRecord::ActiveRecordError
      error_message(action_name)
    rescue StandardError
      render status: 500, json: { error_code: 1, error_message: "サーバー内で不明なエラーが発生しました" }
  end

  def update
    @todo.update!(todo_params)
    successful(@todo, action_name)
    rescue ActiveRecord::RecordInvalid
      error_validate()
    rescue ActiveRecord::ActiveRecordError
      error_message(action_name)
    rescue StandardError
      render status: 500, json: { error_code: 1, error_message: "サーバー内で不明なエラーが発生しました" }
  end

  def destroy
    @todo.destroy!
    successful(@todo, action_name)
    rescue ActiveRecord::RecordInvalid
      error_validate()
    rescue ActiveRecord::ActiveRecordError
      error_message(action_name)
    rescue StandardError
      render status: 500, json: { error_code: 1, error_message: "サーバー内で不明なエラーが発生しました" }
  end

  private

  def todo_params
    params.require(:todo).permit(:title, :detail, :date)
  end

  def set_todo
    @todo = Todo.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      error_validate()
    rescue ActiveRecord::RecordInvalid
      error_validate()
    rescue ActiveRecord::ActiveRecordError
      error_message(action_name)
    rescue StandardError
      render status: 500, json: { error_code: 1, error_message: "サーバー内で不明なエラーが発生しました" }
  end
  
end
