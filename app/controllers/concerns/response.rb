module Response

  def successful(object, action_name)
    if action_name == "index"
      render status: 200, json: { error_code: 0, error_message: "", todos: object, status: status }
    else
      render status: 200, json: { error_code: 0, error_message: "" }
    end
  end

  def error_message(action_name)
    case action_name
    when "index"
      render status: 500, json: { error_code: 3, error_message: "一覧の取得に失敗しました" }
    when "create"
      render status: 500, json: { error_code: 4, error_message: "登録に失敗しました" }
    when "update"
      render status: 500, json: { error_code: 5, error_message: "更新に失敗しました" }
    when "destroy"
      render status: 500, json: { error_code: 6, error_message: "削除に失敗しました" }
    end
  end

  def error_validate
    render status: 400, json: { error_code: 2, error_message: "リクエストの形式が不正です" }
  end
  
end