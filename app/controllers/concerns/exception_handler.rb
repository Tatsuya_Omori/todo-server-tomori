module ExceptionHandler
  # provides the more graceful `included` method
  extend ActiveSupport::Concern

  included do
    # rescue_from StandardError do |e|
    #   render status: 500, json: { error_code: 1, error_message: "サーバー内で不明なエラーが発生しました" }
    # end

    rescue_from ActiveRecord::RecordNotFound do |e|
      render status: 400, json: { error_code: 2, error_message: "リクエストの形式が不正です" }
    end

    # rescue_from ActiveRecord::RecordInvalid do |e|
    #   render json: { error_code: 2, error_message: "リクエストの形式が不正ですinv" }
    # end

  end
end