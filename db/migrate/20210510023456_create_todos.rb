class CreateTodos < ActiveRecord::Migration[6.1]
  def change
    create_table :todos, id: :bigint, unsigned: true do |t|
      t.string :title, null: false, :limit => 100
      t.string :detail, :limit => 1000
      t.date :date

      t.timestamps
    end
  end
end
