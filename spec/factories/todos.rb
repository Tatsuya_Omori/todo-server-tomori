FactoryBot.define do
  factory :todo do
    title { Faker::Lorem.word }
    detail { Faker::Address.name }
    date { Faker::Time }
    created_at { Time.now }
    updated { Time.now }
  end
end
